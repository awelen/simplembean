package net.welen.mbeantest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;

import java.io.IOException;

public class SimpleMBeanServlet extends HttpServlet implements SimpleMBeanServletMBean {
	private static final long serialVersionUID = 1L;	

	private MBeanServer server;	
	private ObjectName objectName;

	@Override
	public void init() throws ServletException {
		server = ManagementFactory.getPlatformMBeanServer();

		try {
			objectName = new ObjectName("welle:type=test");
			server.registerMBean(this, objectName);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	@Override
	public void destroy() {
		try {
			server.unregisterMBean(objectName);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do nothing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do nothing
	}

}
